const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/home');
const sekodlahRouter = require('./routes/sekodlah');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/sekodlah', sekodlahRouter);

app.use((error, req, res, next) => {
  console.log("Error Found")
  console.log('Path: ', req.path)
  console.error('Error: ', error)
 
  if (error.type == 'redirect')
      res.redirect('/error')

   else if (error.type == 'time-out') // arbitrary condition check
      res.status(408).json({
        status: false,
        message: 'request time out'
      })
  else
      res.status(500).json({
        status: false,
        message: error.message
      })
})

module.exports = app;
