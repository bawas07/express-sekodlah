const express = require('express');
const router = express.Router();

/* GET users listing. */
router.get('/hello', function(req, res, next) {
  const name = req.query.name || 'Sekodlah'
  res.json({
    status: true,
    message: 'Hello ' + name
  });
});
/* GET users listing. */
router.get('/hello/:name', function(req, res, next) {
  const name = req.params.name 
  res.json({
    status: true,
    message: 'Hello ' + name
  });
});
/* GET users listing. */
router.post('/hello', function(req, res, next) {
  const name = req.body.name
  if (!name) {
    return res.status(400).json({
      status: false,
      message: 'name is required'
    })
  }
  res.json({
    status: true,
    message: 'Hello ' + name
  });
});


module.exports = router;
