const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  return res.json({
    status: true,
    message: 'up and running!'
  });
});

module.exports = router;
